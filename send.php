<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

if ( ( isset($_POST['phone']) && !empty($_POST['phone']) ) ||
     ( isset($_POST['wapp']) && !empty($_POST['wapp']) )  ) {

    $has_name;
    $has_select_has_id;
    $has_wapp;
    $has_tariff;
    $has_msg;
    $has_id;

    $phone = htmlspecialchars($_POST['phone']);

    $cases = array(
        'case-call-1' => 'Даниил hr - кадровое агенство',
        'case-call-2' => 'UDG/УниЦентр - Предоставление помещений для бизнеса',
        'case-call-3' => 'Витязь - Авиакомпания, которая занимается частной перевозкой людей (Камчатка)',
        'case-call-4' => 'Школа Александра Белгорокова',
        'case-call-5' => 'Школа Натальи Деновой - курсы по инвестициям в недвижимость',
        'case-call-6' => 'Алекса',
        'case-sale-1' => 'Рагнар - Аренда строительной техники',
        'case-sale-2' => 'СНЕК ПРО',
        'case-sale-3' => 'Обход секретаря (торгово-производственная компания)',
        'case-market-1' => 'СК10 - строительная компания',
        'case-qual-1' => 'Зоомагазин "Старая ферма" - интернет-магазин товаров для животных',
        'case-qual-2' => 'Гараж - сеть автосервисов',
        'case-hr-1' => 'бот Za|bota',
        'case-hr-2' => 'Тиарпроджект - Маркетинговое агентство',
        'case-other-1' => 'ГетПрава - автошкола',
        'case-other-2' => 'ТрансАвто - Сервис такси (таксопарк)',
        'case-rest-1' => 'ООО "Профгрупп"',
        'case-rest-2' => 'ООО "Геоиннотерн" - космоснимки, картография, обработка радарных данных',
        'case-rest-3' => 'ООО Виномаркет "Точка" - Сеть винотек в Крыму (розничная продажа алкоголя)',
        'case-rest-4' => 'ЕЦР - единый центр разнорабочих, позволяет объединить рабочих и работодателей',
        'case-rest-5' => 'Аукцион - продажа лидов',
        'faq' => 'ЧаВо',
        'prices' => 'Таблица цен',
    );

    $rates = array(
        'rate__tariff-gift' => 'Подарок',
        'rate__tariff-min' => 'Minimum',
        'rate__tariff-max' => 'Maximum',
        'rate__tariff-pro' => 'Pro',
    );

    $forms = array(
        'modal_call-order-1' => 'Оставьте свой контакт',
        'modal_call-order-2' => 'Протестировать бота',
        'modal_call-order-3' => 'Оставьте ваш контакт + сообщение',
    );


    if (isset($_POST['name']) && !empty($_POST['name'])) {
        $name = htmlspecialchars($_POST['name']);

        $has_name = true;
    }
    if (isset($_POST['select']) && !empty($_POST['select'])) {
        $select = htmlspecialchars($_POST['select']);

        $has_select = true;
    }
    if (isset($_POST['wapp']) && !empty($_POST['wapp'])) {
        $wapp = htmlspecialchars($_POST['wapp']);
        $has_wapp = true;
    }
    if (isset($_POST['rate']) && !empty($_POST['rate'])) {
        $rate = htmlspecialchars($rates[$_POST['rate']]);
        $has_tariff = true;
    }
    if (isset($_POST['msg']) && !empty($_POST['msg'])) {
        $msg = htmlspecialchars($_POST['msg']);
        $has_msg = true;
    }

    if (isset($_POST['msg']) && !empty($_POST['msg'])) {
        $msg = htmlspecialchars($_POST['msg']);
        $has_msg = true;
    }

    if (isset($_POST['id']) && !empty($_POST['id']) ) {
        $id = htmlspecialchars($cases[$_POST['id']]);
        $has_id = true;
    }

    if (isset($_POST['modal']) && !empty($_POST['modal']) ) {
        $form = htmlspecialchars($forms[$_POST['modal']]);
        $has_form = true;
    }

    try {
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = 'smtp.beget.com';                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'zabota@busupdev.ru';                     //SMTP username
        $mail->Password   = 'IPW*2uiM';                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
        $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
        $mail->CharSet = 'utf-8';
        //Recipients
        $mail->setFrom('zabota@busupdev.ru', 'ZaBota');
        $mail->addAddress('fastdienicetry@gmail.com', 'Joe User');     //Add a recipient
        $mail->addAddress('project@business-up.org');               //Name is optional
        $mail->addReplyTo('zabota@busupdev.ru');
        $mail->addCC('zabota@busupdev.ru');
        $mail->addBCC('zabota@busupdev.ru');

        //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = 'Письмо с сайта';
        $mail->Body = '';
        if ( $has_name ) {
            $mail->Body    .= '<p>Имя: '. $name .'</p>';
        }

        if ( $has_form ) {
            $mail->Body    .= '<p>Название формы: '. $form .'</p>';
        }
        
        if ( $has_id ) {
            $mail->Body    .= '<p>Страница: '. $id .'</p>';
        }

            
        if ($phone) {
            $mail->Body    .= '<p>Телефон: '. $phone .'</p>';
        }



        if ( $has_select ) {
            $mail->Body    .= '<p>Выбранный бот: '. $select .'</p>';
        }
        
        if ( $has_wapp ) {
            $mail->Body    .= '<p>Whatsapp: '. $wapp .'</p>';
        }
        
        if ($has_tariff) {
            $mail->Body    .= '<p>Тариф: '. $rate .'</p>';
        }
        
        if ($has_msg) {
            $mail->Body    .= '<p>Сообщение: '. $msg .'</p>';
        }
        
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        $obj = array(
            "success" => true,
            "data" => array(
                "text"=> "text"
            )
        );

        echo json_encode($obj);

    } catch (Exception $e) {
        echo json_encode("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
    }
}